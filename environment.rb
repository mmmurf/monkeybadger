require "bundler/setup"
require "digest/sha1"
require "json"

Bundler.require
require File.expand_path(File.dirname(__FILE__) + "/src/models.rb")
require File.expand_path(File.dirname(__FILE__) + "/src/services.rb")
require File.expand_path(File.dirname(__FILE__) + "/src/app.rb")
require File.expand_path(File.dirname(__FILE__) + "/main.rb")

case ENV['RACK_ENV']

when 'development'

  REDIS_HOST = 'localhost'
  REDIS_PORT = '6379'
  REDIS_PASSWORD = nil

  HTTP_AUTH_USERNAME = 'chloe'
  HTTP_AUTH_PASSWORD = 'is a bell'

  # # clear and seed error data
  # MonkeyBadger::Data.clear!
  # MonkeyBadger::Data.import! File.expand_path(File.dirname(__FILE__) + "/data.json")

when 'production'

  uri = URI.parse(ENV["REDISCLOUD_URL"])
  REDIS_HOST = uri.host
  REDIS_PORT = uri.port
  REDIS_PASSWORD = uri.password

  HTTP_AUTH_USERNAME = ENV['HTTP_AUTH_USERNAME']
  HTTP_AUTH_PASSWORD = ENV['HTTP_AUTH_PASSWORD']

  Pony.options = {
    via: :smtp,
    via_options: { 
      address: 'smtp.sendgrid.net', 
      port: '587',
      domain: 'heroku.com',
      user_name: ENV['SENDGRID_USERNAME'], 
      password: ENV['SENDGRID_PASSWORD'], 
      authentication: :plain, 
      enable_starttls_auto: true
    }
  }

end